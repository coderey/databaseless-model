<?php

namespace Coderey\DatabaseLessModel;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use InvalidArgumentException;

/**
 * this is my new base-class for rapidly-created database-less models
 *
 * - extend from this class
 * - define $data-array ('key' => 'default-value') where keys are snake-case
 * - define setter- and getter-methods via annotations (camel-case)
 *
 * - enjoy the magic!
 */
abstract class DatabaseLessModel
{
    protected array $data               = [];
    protected array $castedData         = [];
    protected array $validationRules    = [];
    protected array $validationMessages = [];
    protected array $casts              = [];

    /**
     * @throws ValidationException
     */
    public function __construct(array $data = [])
    {
        $errors = [];
        foreach ($data as $key => $value) {
            try {
                $this->setAttribute($key, $value);
            } catch (ValidationException $exception) {
                $errors = array_merge($errors, $exception->errors());
            }
        }

        if ( !empty($errors)) {
            throw ValidationException::withMessages($errors);
        }
    }

    public function __call(string $method, array $params): mixed
    {
        if (preg_match('/set(?<param>[A-Z].*)/', $method, $out)) {
            $key   = Str::snake($out['param']);
            $value = null;
            if (array_key_exists(0, $params)) {
                $value = $params[0];
            }

            return $this->setAttribute($key, $value);
        }

        if (preg_match('/get(?<param>[A-Z].*)/', $method, $out)) {
            $key = Str::snake($out['param']);

            return $this->getAttribute($key);
        }

        throw new InvalidArgumentException('invalid method "' . $method . '"');
    }

    /**
     * @throws ValidationException
     */
    protected function setAttribute(string $key, mixed $value): static
    {
        if (array_key_exists($key, $this->data)) {
            $validated        = $this->validate($key, $value);
            $this->data[$key] = $validated[$key];
            $this->castData($key, $this->data[$key]);

            return $this;
        }

        throw new InvalidArgumentException('attribute "' . $key . '" does not exist.');
    }

    protected function getAttribute(string $key): mixed
    {
        if (array_key_exists($key, $this->castedData)) {
            return $this->castedData[$key];
        }

        if (array_key_exists($key, $this->data)) {
            return $this->data[$key];
        }

        throw new InvalidArgumentException('attribute "' . $key . '" does not exist.');
    }

    /**
     * validate either a single data-field or the whole object (if no $key / $value given)
     *
     * @throws ValidationException
     */
    public function validate(string $key = null, mixed $value = null): array
    {
        $data  = $this->data;
        $rules = $this->validationRules;

        if ($key !== null) {
            $data  = [$key => $value];
            $rules = [$key => 'present'];

            if (array_key_exists($key, $this->validationRules)) {
                $rules[$key] = $this->validationRules[$key];
            }
        } else {
            foreach ($this->data as $key => $value) {
                if ( !array_key_exists($key, $rules)) {
                    $rules[$key] = 'present'; // can be empty
                }
            }
        }

        $validator = Validator::make($data, $rules, $this->validationMessages);

        return $validator->validate();
    }

    protected function castData($key, $value)
    {
        $cast = array_key_exists($key, $this->casts) ? $this->casts[$key] : '';

        switch ($cast) {
            case 'datetime':
            case 'date_time':
                $this->castedData[$key] = new Carbon($value);
                break;
            case 'date':
                $this->castedData[$key] = (new Carbon($value))->startOfDay();
                break;
            case 'string':
                $this->castedData[$key] = (string) $value;
                break;
            case 'int':
            case 'integer':
                $this->castedData[$key] = (int) $value;
                break;
            case 'float':
            case 'decimal':
                $this->castedData[$key] = (float) $value;
                break;
            case 'array':
                if (is_iterable($value)) {
                    $this->castedData[$key] = [];
                    foreach ($value as $index => $item) {
                        $this->castedData[$key][$index] = $item;
                    }
                } else {
                    $this->castedData[$key] = (array) $value;
                }
                break;
        }
    }
}
