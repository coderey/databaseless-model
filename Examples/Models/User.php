<?php

namespace Examples\Coderey\DatabaseLessModel\Models;

use Coderey\DatabaseLessModel\DatabaseLessModel;
use Illuminate\Support\Carbon;

/**
 * @method self setId(int $id)
 * @method int|null getId()
 * @method self setFirstName(string $name)
 * @method string getFirstName()
 * @method self setLastName(string $name)
 * @method string getLastName()
 * @method self setBirthday(Carbon|string $name)
 * @method Carbon|null getBirthday()
 */
class User extends DatabaseLessModel
{
    protected array $data = [
        'id'         => null,
        'first_name' => '',
        'last_name'  => '',
        'birthday'   => null,
    ];

    protected array $validationRules = [
        'id'         => 'required|int',
        'first_name' => 'required|min:2',
        'last_name'  => 'required|min:2',
    ];

    protected array $casts = [
        'birthday' => 'date',
        'id'       => 'int',
    ];

}