<?php

namespace Examples\Coderey\DatabaseLessModel\Models;

use Coderey\DatabaseLessModel\DatabaseLessModel;
use Illuminate\Support\Carbon;

/**
 * @method self setInt(int $value)
 * @method int getInt()
 * @method self setInteger(int $value)
 * @method int getInteger()
 * @method self setFloat(float $value)
 * @method float getFloat()
 * @method self setDecimal(float $value)
 * @method float getDecimal()
 * @method self setString(string $name)
 * @method string getString()
 * @method self setDate(Carbon|string $name)
 * @method Carbon|null getDate()
 * @method self setDateTime(Carbon|string $name)
 * @method Carbon|null getDateTime()
 * @method self setArray(array|mixed $name)
 * @method array getArray()
 */
class CastingTestModel extends DatabaseLessModel
{
    protected array $data = [
        'int'       => 0,
        'integer'   => 0,
        'float'     => 0.0,
        'decimal'   => 0.0,
        'string'    => '',
        'date'      => null,
        'date_time' => null,
        'datetime'  => null,
        'array'     => [],
    ];

    protected array $casts = [
        'int'       => 'int',
        'integer'   => 'integer',
        'float'     => 'float',
        'decimal'   => 'decimal',
        'string'    => 'string',
        'date'      => 'date',
        'date_time' => 'date_time',
        'datetime'  => 'datetime',
        'array'     => 'array',
    ];

}