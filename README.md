# DatabaseLess Model

This is my base-class for data-containers without database-access - e.g. if I need a well-defined data-container as result of an API.

 - extend from this class
 - define $data-array ('key' => 'default-value') where keys are snake-case
 - define setter- and getter-methods via annotations (camel-case)

***enjoy the magic!***


extended magic:

if you want to validate any input or define user-defined accessors: you can use laravel-like accessors and mutators
e.g. if you have a data-field 'date_start' and you want to validate it e.g. via regex, you can write a method
setDateStart(string $dateStart) where you validate your input and then store it into `$this->data['date_start']`

## Code-Example

```php
<?php

namespace ExampleProject\Models;

use Coderey\DatabaseLessModel\DatabaseLessModel;

/**
 * @method self setId(int $id)
 * @method int getId()
 * @method self setFirstName(string $firstName)
 * @method int getId()
 */
class User extends DatabaseLessModel
{
    protected array $data = [
        'id' => 0
        'first_name' => '',
        'last_name' => '',
    ];

}
```
