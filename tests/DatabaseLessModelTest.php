<?php

namespace Tests\Coderey\DatabaseLessModel;

use DateTime;
use Examples\Coderey\DatabaseLessModel\Models\CastingTestModel;
use Examples\Coderey\DatabaseLessModel\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Validation\ValidationException;
use Orchestra\Testbench\TestCase;

/**
 * @covers \Coderey\DatabaseLessModel\DatabaseLessModel
 * @covers \Examples\Coderey\DatabaseLessModel\Models\User
 */
class DatabaseLessModelTest extends TestCase
{
    public function testSetDataBySettersAndCheckWithGetters(): void
    {
        $user = new User();
        $user->setId(2);
        $user->setFirstName('Hans');
        $user->setLastName('Wurst');

        self::assertEquals(2, $user->getId());
        self::assertEquals('Hans', $user->getFirstName());
        self::assertEquals('Wurst', $user->getLastName());
    }

    /**
     * @throws ValidationException
     */
    public function testSetDataByConstructorAndCheckWithGetters(): void
    {
        $user = new User(
            [
                'id'         => 3,
                'first_name' => 'Anna',
                'last_name'  => 'Bolika',
            ]
        );

        self::assertEquals(3, $user->getId());
        self::assertEquals('Anna', $user->getFirstName());
        self::assertEquals('Bolika', $user->getLastName());
    }

    public function validationDataProvider(): array
    {
        return [
            [
                'input'                  => [],
                'errorsAfterConstructor' => [],
                'errorsAfterValidation'  => ['id', 'first_name', 'last_name'],
            ],
            [
                'input'                  => ['id' => null, 'first_name' => 'M', 'last_name' => 'J'],
                'errorsAfterConstructor' => ['id', 'first_name', 'last_name'],
                'errorsAfterValidation'  => [],
            ],
            [
                'input'                  => ['id' => null, 'first_name' => 'Mary', 'last_name' => 'Juhanna'],
                'errorsAfterConstructor' => ['id'],
                'errorsAfterValidation'  => [],
            ],
        ];
    }

    /**
     * @dataProvider validationDataProvider
     */
    public function testValidationWillFail(array $input, array $errorsAfterConstructor, array $errorsAfterValidation): void
    {
        $exception = null;
        $user      = null;

        try {
            $user = new User($input);
        } catch (\Throwable $ex) {
            $exception = $ex;
        }

        if ( !empty($errorsAfterConstructor)) {
            self::assertInstanceOf(ValidationException::class, $exception);
            $this->checkForErrors($exception, $errorsAfterConstructor);
            self::assertNull($user);
        } else {
            self::assertNull($exception);

            $exception = null;
            try {
                $validated = $user->validate();
            } catch (\Throwable $ex) {
                $exception = $ex;
            }

            if ( !empty($errorsAfterValidation)) {
                self::assertInstanceOf(ValidationException::class, $exception);
                $this->checkForErrors($exception, $errorsAfterValidation);
            } else {
                self::assertNull($exception);
            }
        }
    }

    protected function checkForErrors(ValidationException $exception, array $expectedErrors): void
    {
        $allFields      = ['id', 'first_name', 'last_name'];
        $nonErrorFields = array_diff($allFields, $expectedErrors);

        $errors = $exception->errors();
        self::assertIsArray($errors);

        foreach ($expectedErrors as $errorField) {
            self::assertArrayHasKey($errorField, $errors);
        }

        foreach ($nonErrorFields as $errorField) {
            self::assertArrayNotHasKey($errorField, $errors);
        }
    }

    /**
     * @throws ValidationException
     */
    public function testCastingSomeInput(): void
    {
        $user = new User(['id' => '123', 'birthday' => '11.05.1990']);
        self::assertSame(123, $user->getId());
        self::assertInstanceOf(Carbon::class, $user->getBirthday());
        self::assertSame('1990-05-11 00:00:00', $user->getBirthday()->format('Y-m-d H:i:s'));
    }

    /**
     * @covers \Examples\Coderey\DatabaseLessModel\Models\CastingTestModel
     */
    public function testStringCastings(): void
    {
        $model = new CastingTestModel();
        self::assertSame('', $model->getString());
        $model->setString('Hallo Du!');
        self::assertSame('Hallo Du!', $model->getString());
        $model->setString(12345);
        self::assertSame('12345', $model->getString());
        $model->setString(new Carbon('12.05.2020 20:15'));
        self::assertSame('2020-05-12 20:15:00', $model->getString());
    }

    /**
     * @covers \Examples\Coderey\DatabaseLessModel\Models\CastingTestModel
     */
    public function testIntegerCastings(): void
    {
        $model = new CastingTestModel();
        self::assertSame(0, $model->getInt());
        self::assertSame(0, $model->getInteger());
        $model->setInt(1);
        $model->setInteger(2);
        self::assertSame(1, $model->getInt());
        self::assertSame(2, $model->getInteger());
        //round/floor float
        $model->setInt(3.4);
        $model->setInteger(3.2);
        self::assertSame(3, $model->getInt());
        self::assertSame(3, $model->getInteger());
        $model->setInt(3.7);
        $model->setInteger(3.8);
        self::assertSame(3, $model->getInt());
        self::assertSame(3, $model->getInteger());
        $model->setInt(3.9);
        $model->setInteger(3.99999999999999999999999999999999999); // Angabe > max. Genauigkeit von PHP --> PHP rundet auf 4.0
        self::assertSame(3, $model->getInt());
        self::assertSame(4, $model->getInteger());
        $model->setInt('1');
        $model->setInteger('2');
        self::assertSame(1, $model->getInt());
        self::assertSame(2, $model->getInteger());
        //round/floor float
        $model->setInt('3.4');
        $model->setInteger('3.2');
        self::assertSame(3, $model->getInt());
        self::assertSame(3, $model->getInteger());
        $model->setInt('3.7');
        $model->setInteger('3.8');
        self::assertSame(3, $model->getInt());
        self::assertSame(3, $model->getInteger());
        $model->setInt('3.9');
        $model->setInteger('3.99999999999999999999999999999999999'); // lesson learned: auch der String wird erst als Float geparst und dann zum Integer...
        self::assertSame(3, $model->getInt());
        self::assertSame(4, $model->getInteger());
        $model->setInt('35e2');    // PHP erkennt den String als Expotential-Schreibweise
        $model->setInteger('35a'); // hier werden nur die Zahlen bis zum ersten alphanum-Zeichen genommen...
        self::assertSame(3500, $model->getInt());
        self::assertSame(35, $model->getInteger());
        $model->setInt('0b0100');    // binary wird als String nicht erkannt --> 0
        $model->setInteger('0x1A');  // hex ebenfalls nicht --> 0
        self::assertSame(0, $model->getInt());
        self::assertSame(0, $model->getInteger());

        $model->setInt('0755');      //die führende 0 wird nicht betrachtet / nicht als Oktal gesehen
        $model->setInteger('0o755'); // und auch diese Schreibweise greift nicht...
        self::assertSame(755, $model->getInt());
        self::assertSame(0, $model->getInteger());

        $model->setInt(0b0100);     // real binary - wird intern gecastet
        $model->setInteger(0xFF);   // hex - intern gecastet
        self::assertSame(4, $model->getInt());
        self::assertSame(255, $model->getInteger());
        $model->setInt(0755);      //oktal
        self::assertSame(493, $model->getInt());
    }

    /**
     * @covers \Examples\Coderey\DatabaseLessModel\Models\CastingTestModel
     */
    public function testFloatCastings(): void
    {
        $model = new CastingTestModel();
        self::assertSame(0.0, $model->getFloat());
        self::assertSame(0.0, $model->getDecimal());
        $model->setFloat(1);
        $model->setDecimal(2);
        self::assertSame(1.0, $model->getFloat());
        self::assertSame(2.0, $model->getDecimal());
        //round/floor float
        $model->setFloat(3.4);
        $model->setDecimal(3.2);
        self::assertSame(3.4, $model->getFloat());
        self::assertSame(3.2, $model->getDecimal());
        $model->setFloat(3.7);
        $model->setDecimal(3.8);
        self::assertSame(3.7, $model->getFloat());
        self::assertSame(3.8, $model->getDecimal());
        $model->setFloat(3.9);
        $model->setDecimal(3.99999999999999999999999999999999999); // Angabe > max. Genauigkeit von PHP --> PHP rundet auf 4.0
        self::assertSame(3.9, $model->getFloat());
        self::assertSame(4.0, $model->getDecimal());
        $model->setFloat('1');
        $model->setDecimal('2');
        self::assertSame(1.0, $model->getFloat());
        self::assertSame(2.0, $model->getDecimal());
        //round/floor float
        $model->setFloat('3.4');
        $model->setDecimal('3,2'); // Komma erkennt er nicht...
        self::assertSame(3.4, $model->getFloat());
        self::assertSame(3.0, $model->getDecimal());
        $model->setFloat('3.7');
        $model->setDecimal('3,8'); // Komma erkennt er nicht...
        self::assertSame(3.7, $model->getFloat());
        self::assertSame(3.0, $model->getDecimal());
        $model->setFloat('3.9');
        $model->setDecimal('3.99999999999999999999999999999999999');
        self::assertSame(3.9, $model->getFloat());
        self::assertSame(4.0, $model->getDecimal());
        $model->setFloat('35e2');    // PHP erkennt den String als Expotential-Schreibweise
        $model->setDecimal('35a');   // hier werden nur die Zahlen bis zum ersten alphanum-Zeichen genommen...
        self::assertSame(3500.0, $model->getFloat());
        self::assertSame(35.0, $model->getDecimal());
        $model->setFloat('35e-2');    // PHP erkennt den String als Expotential-Schreibweise
        $model->setDecimal('2^3');    // Potenzen versteht er nicht...
        self::assertSame(0.35, $model->getFloat());
        self::assertSame(2.0, $model->getDecimal());
        $model->setFloat('0b0100');    // binary wird als String nicht erkannt --> 0
        $model->setDecimal('0x1A');    // hex ebenfalls nicht --> 0
        self::assertSame(0.0, $model->getFloat());
        self::assertSame(0.0, $model->getDecimal());

        $model->setFloat('0755');      //die führende 0 wird nicht betrachtet / nicht als Oktal gesehen
        $model->setDecimal('0o755');   // und auch diese Schreibweise greift nicht...
        self::assertSame(755.0, $model->getFloat());
        self::assertSame(0.0, $model->getDecimal());

        $model->setFloat(0b0100);     // real binary - wird Floatern gecastet
        $model->setDecimal(0xFF);     // hex - Floatern gecastet
        self::assertSame(4.0, $model->getFloat());
        self::assertSame(255.0, $model->getDecimal());
        $model->setFloat(0755);      //oktal
        self::assertSame(493.0, $model->getFloat());
    }

    /**
     * @covers \Examples\Coderey\DatabaseLessModel\Models\CastingTestModel
     */
    public function testDateCastings(): void
    {
        $model = new CastingTestModel();
        self::assertSame(null, $model->getDate());
        self::assertSame(null, $model->getDateTime());

        $model->setDate('2022-05-03 12:34:56');
        $model->setDateTime('2022-05-03 12:34:56');
        self::assertInstanceOf(Carbon::class, $model->getDate());
        self::assertInstanceOf(Carbon::class, $model->getDateTime());
        self::assertSame('2022-05-03 00:00:00', $model->getDate()->format('Y-m-d H:i:s'));
        self::assertSame('2022-05-03 12:34:56', $model->getDateTime()->format('Y-m-d H:i:s'));

        $model->setDate(new DateTime('2022-05-03 12:34:56'));
        $model->setDateTime(new DateTime('2022-05-03 12:34:56'));
        $model->setDatetime(new DateTime('2022-05-03 12:34:56'));
        self::assertInstanceOf(Carbon::class, $model->getDate());
        self::assertInstanceOf(Carbon::class, $model->getDateTime());
        self::assertInstanceOf(Carbon::class, $model->getDatetime());
        self::assertSame('2022-05-03 00:00:00', $model->getDate()->format('Y-m-d H:i:s'));
        self::assertSame('2022-05-03 12:34:56', $model->getDateTime()->format('Y-m-d H:i:s'));
        self::assertSame('2022-05-03 12:34:56', $model->getDatetime()->format('Y-m-d H:i:s'));
    }

    /**
     * @covers \Examples\Coderey\DatabaseLessModel\Models\CastingTestModel
     */
    public function testArrayCastings(): void
    {
        $model = new CastingTestModel();
        self::assertSame([], $model->getArray());
        $model->setArray([1, 2, 4]);
        self::assertSame([1, 2, 4], $model->getArray());
        $model->setArray(['1', '2', '4']);
        self::assertSame(['1', '2', '4'], $model->getArray());
        $model->setArray([0 => '1', 1 => '2', 5 => '4', '3' => 12, 4 => '18']);
        self::assertSame([0 => '1', 1 => '2', 5 => '4', '3' => 12, 4 => '18'], $model->getArray());
        $model->setArray('foo');
        self::assertSame(['foo'], $model->getArray());
        $model->setArray(new Collection(['foo', 'bar']));
        self::assertSame(['foo', 'bar'], $model->getArray());
        $model->setArray(collect(['foo', 'bar']));
        self::assertSame(['foo', 'bar'], $model->getArray());
    }

}
